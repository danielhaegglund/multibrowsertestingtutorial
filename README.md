# MultiBrowserTestingTutorial
This project demonstrates how multibrowser testing can be achieved by using Selenium Grid and Docker

# Preparations
In order to use this project you need to download Docker for the platform you use:
* Docker for Mac
* Docker for Windows

You also need to have a .NET-IDE like Visual Studio, Rider or Xamarin Studio installed running either mono or .net.

# The website under test
The example project is a very simple website that will let the user enter a string and get the reversed string returned. The site will be run on nginx inside a docker container. You can review the Dockerfile to see how the image is populated with data from the project. 
You can examine the code for the site in the project "StringReverserWebsite". Although it is a console app you cannot run it by starting the app since it should run in a docker container. You can however run the site by opening the index.html in your local browser.

# The tests
The tests are located in the project "SeleniumMultiBrowserTesting". There is a base class "TestBase" that contains boilerplate code like a CapabilityFactory that stores the capabilites of the web drivers to enable running the tests in either chrome or firefox. 
It also contains code for setting up screenshoot storage so that if a test runs into an exception we can see what happend. Last is a wrapper function that deals with the creating a suitable web driver with the right capabilities, executing the tests and cleaning up afterwards. 

The actual tests themselves can be found in the class "BrowserTests". It inherits the TestBase class. Each test will be run for each set of capabilites that are specified in the CapabillityFactory. The TestCaseSource will iterate the factory and execute the test. 
The tests are run in the baseclass TestAction. To drive the tests, a page object is used that represents the page we want to test and contains all the Selenium calls that automates interactions with the page. Page objects is a pattern to seperate the automation of the browsers from the test code.

# The docker-compose file
The docker-compose.yml file describes the infrastructure for setting up the nginx webserver, the selenium grid hub, a selenium node 
running chrome and a selenium node running firefox. The selenium nodes have VNC servers so if you want to see the tests running in 
the browsers you just connect to them with a VNC client (password is "secret" for both nodes).
* Chrome node VNC port is 5910
* Firefox node VNC port is 5920

# Start the environment
To start all the necessary containers you just navigate to the folder containing the docker-compose.yml file from a terminal (or 
PowerShell if your'e using Windows). Then enter the command

__docker-compose build__

This will build the nginx webserver image and populate it with data. If you change any data or remove the images you will need to run this command again.

Now you're ready to start the containers by entering the command:

__docker-compose up -d__

This will start the containers in daemon mode. First time you run the docker-compose file it will download images for Selenium Grid Hub, Selenium Node Chrome, 
Selenium node Firefox and nginx from Docker Hub. The images will be stored locally on your machine so next time you start run the docker-compose file it will 
be much faster.

# Trying the site out from your local browser
Begin by opening the site in any browser on your local machine by navigating to 'http://localhost:8010'.

# Verify that Selenium Grid is up and running
To verify that Selenium Grid is up and running navigate to 'http://localhost:4444/grid/console'. The page should show that there are two nodes running (Chrome and Firefox).

# Running the tests
To run the tests just open your preferred IDE and run the unit tests. If you connect to the nodes through VNC you will be able to see the tests running in the browsers.

# Stopping the environment
To stop the environment when the tests are done, enter the command:

__docker-compose down__

# Discussion
A benefit of using Docker Containers for setting up a Selenium Grid is that anyone can run the tests without having to install the Selenium Server, web drivers, different browsers etc. As long as Docker is installed you are good to go.
After the test have been run all the containers are disposed of and the only thing remaining on your computer is the docker images for Selenium and nginx. These can be removed with the command __docker rmi <imageid>__ to save disk space.

Since the Selenium Grid and the Selenium node are based on Linux images it will limit the browsers that can be used. There are docker images for Chrome and Firefox Selenium nodes but there are no images for Microsoft Edge, Internet Explorer or
Safari since these require different operating systems to run. You can however set up these nodes on other machines and connect to them form Selenium hub running inside docker but it will make the setup more fragile since it depends on external resources.

The structure of the tests with a base class that handles boilerplate code and sub classes that implements the tests using page objects to interact with the browsers makes it quite clean. We don't need to duplicate test code for different browsers.
Using the TestCaseSource-attribute will run the tests for all specified capabilities (browsers). The drawback of this solution is that the tests will run in sequence. Since the tests take quite a long time to execute it would be preferable to run them in parallel but that will not work in this case. 
