﻿using NUnit.Framework;
using OpenQA.Selenium.Remote;
using FluentAssertions;

namespace SeleniumMultiBrowserTesting
{
    [TestFixture]
    public class BrowserTests : TestBase
    {
        [Test, TestCaseSource(typeof(CapabilityFactory), nameof(CapabilityFactory.Capabilities))]
        public void GoToHomePage(DesiredCapabilities capabilities)
        {
            TestAction((webDriver) =>
            {
                // Arrange
                var page = new PageObjects.StringReverserPage(webDriver);

                // Act
                page.GotoPage("http://StringReverserWebSite");

                // Assert
                page.Title.Should().Be("String Reverser");
            }, capabilities, "GotoHomePage");
        }

        [Test, TestCaseSource(typeof(CapabilityFactory), nameof(CapabilityFactory.Capabilities))]
        public void StringShouldBeReversed(DesiredCapabilities capabilities)
        {
            TestAction((webDriver) =>
                {
                    // Arrange
                    var page = new PageObjects.StringReverserPage(webDriver);

                    // Act
                    page.GotoPage("http://StringReverserWebSite").EnterString("abc");

                    // Assert
                    page.ReversedString.Should().Be("cba");
                    page.IsPalindrome.Should().BeFalse();
                }, capabilities, "StringShouldBeReversed"
            );
        }

        [Test, TestCaseSource(typeof(CapabilityFactory), nameof(CapabilityFactory.Capabilities))]
        public void PalindromeShouldBeIdentified(DesiredCapabilities capabilities)
        {
            TestAction((webDriver) =>
                {
                    // Arrange
                    var page = new PageObjects.StringReverserPage(webDriver);

                    // Act
                    page.GotoPage("http://StringReverserWebSite").EnterString("racecar");

                    // Assert
                    page.ReversedString.Should().Be("racecar");
                    page.IsPalindrome.Should().BeTrue();
                }, capabilities, "PalindromeShouldBeIdentified"
            );
        }
    }
}