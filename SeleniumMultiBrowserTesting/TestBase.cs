﻿using System;
using System.Collections;
using System.IO;
using System.Reflection;
using NUnit.Framework;
using OpenQA.Selenium;
using OpenQA.Selenium.Chrome;
using OpenQA.Selenium.Firefox;
using OpenQA.Selenium.Support.Extensions;
using OpenQA.Selenium.Remote;

namespace SeleniumMultiBrowserTesting
{
    public class CapabilityFactory
    {
        public static IEnumerable Capabilities
        {
            get
            {
                yield return new TestCaseData(new ChromeOptions().ToCapabilities());
                yield return new TestCaseData(new FirefoxOptions().ToCapabilities());
            }
        }
    }

    public abstract class TestBase
    {
        private IWebDriver _webDriver;
        private string _screenshotPath;

        [OneTimeSetUp]
        public void OneTimeSetup()
        {
            var codeBase = Assembly.GetExecutingAssembly().CodeBase;
            var uri = new UriBuilder(codeBase);
            var assemblyPath = Uri.UnescapeDataString(uri.Path);
            var path = Path.GetDirectoryName(assemblyPath);
            _screenshotPath = $"{path}{Path.AltDirectorySeparatorChar}Screenshots{Path.AltDirectorySeparatorChar}";
            if (Directory.Exists(_screenshotPath))
            {
                Directory.Delete(_screenshotPath, true);
            }
            Directory.CreateDirectory(_screenshotPath);   
          
        }

        protected void TestAction(Action<IWebDriver> action, ICapabilities capabilities, string testName)
        {

            try
            {
                _webDriver = new RemoteWebDriver(new Uri("http://localhost:4444/wd/hub"), capabilities);
                action(_webDriver);
            }
            catch (Exception)
            {
                if (_webDriver != null)
                {
                    var screenshot = _webDriver.TakeScreenshot();

                    var filePath = $"{_screenshotPath}{testName}_{capabilities.BrowserName}";

                    screenshot?.SaveAsFile(filePath, ScreenshotImageFormat.Png);
                }
                throw;
            }
            finally
            {
                _webDriver?.Quit();
            }
        }

    }
}