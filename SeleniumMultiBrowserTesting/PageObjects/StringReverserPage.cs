﻿using System;
using OpenQA.Selenium;
using OpenQA.Selenium.Support.UI;

namespace SeleniumMultiBrowserTesting.PageObjects
{
    public class StringReverserPage
    {
        private readonly IWebDriver _webDriver;

        public string Title => _webDriver == null ? string.Empty : _webDriver.Title;
        public string ReversedString => _webDriver == null ? string.Empty : _webDriver.FindElement(By.Id("result")).Text;
        public bool IsPalindrome => _webDriver != null &&
                                    !string.Equals(_webDriver.FindElement(By.Id("palindrome")).GetAttribute("class"),
                                        "hidepalindrome");


        public StringReverserPage(IWebDriver webDriver)
        {
            _webDriver = webDriver;
        }

        public StringReverserPage GotoPage(string url)
        {
            _webDriver.Navigate().GoToUrl(url);
            IWait<IWebDriver> wait = new WebDriverWait(_webDriver, TimeSpan.FromSeconds(10));
            wait.Until(driver1 => ((IJavaScriptExecutor)_webDriver).ExecuteScript("return document.readyState").Equals("complete"));
            return this;
        }

        public StringReverserPage EnterString(string input)
        {
            _webDriver.FindElement(By.Id("normalstring")).SendKeys(input);
            _webDriver.FindElement(By.Id("submitbutton")).Click();
            return this;
        }
    }
}