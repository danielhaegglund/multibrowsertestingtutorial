function reverse()
{
    var normalstring = $('#normalstring').val();
    var reversedstring = normalstring.split('').reverse().join('')

    $('#result').html(reversedstring);
    if(normalstring === reversedstring)
    {
        $('#palindrome').removeClass('hidepalindrome');
    }
    else
    {
        $('#palindrome').addClass('hidepalindrome');
    }

}